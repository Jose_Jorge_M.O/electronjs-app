create database example;
use example;

drop table if exists tasks;
drop table if exists lists;
drop table if exists users;

CREATE TABLE users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50),
    contrasena VARCHAR(100)
);

CREATE TABLE lists (
	id INT PRIMARY KEY AUTO_INCREMENT,
    listname VARCHAR(50) UNIQUE,
    user_id INT,
	FOREIGN KEY (user_id) references users(id)
);

CREATE TABLE tasks (
    id INT AUTO_INCREMENT PRIMARY KEY,
    list_id INT,
    title VARCHAR(255) NOT NULL,
    description TEXT,
    due_date DATE,
    status ENUM('pendiente', 'en progreso', 'completada') DEFAULT 'pendiente',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (list_id) references lists(id)
);

insert into users (username, contrasena) values
("juan", "pass");

insert into lists (listname, user_id) values ("default",2);

INSERT INTO tasks (list_id, title, description, due_date, status) 
VALUES 
	 (2, 'Preparar presentación para reunión de equipo', 'Revisar los datos y preparar diapositivas para la presentación del proyecto.', '2023-09-20', 'pendiente'),
  (2, 'Enviar informe mensual a clientes', 'Generar y enviar el informe de progreso mensual a los clientes principales.', '2023-09-25', 'pendiente'),
  (2, 'Investigar nuevas tendencias de mercado', 'Realizar una investigación sobre las tendencias actuales del mercado y presentar hallazgos.', '2023-09-30', 'en progreso'),
  (2, 'Reunión con el departamento de ventas', 'Coordinar una reunión con el equipo de ventas para discutir estrategias y objetivos.', '2023-10-05', 'pendiente'),
  (2, 'Actualizar la documentación del proyecto', 'Revisar y actualizar la documentación del proyecto con los últimos cambios y mejoras.', '2023-10-10', 'pendiente'),
  (2, 'Entrenamiento de nuevos empleados', 'Conducir sesiones de entrenamiento para los nuevos miembros del equipo.', '2023-10-15', 'pendiente'),
  (2, 'Revisar propuestas de proveedores', 'Analizar y evaluar las propuestas de los proveedores para la próxima adquisición de equipos.', '2023-10-20', 'pendiente'),
  (2, 'Realizar pruebas de rendimiento del software', 'Conducir pruebas de rendimiento en el nuevo software antes del lanzamiento.', '2023-10-25', 'pendiente'),
  (2, 'Preparar presentación para conferencia', 'Preparar la presentación y los materiales necesarios para la próxima conferencia de la industria.', '2023-10-30', 'pendiente'),
  (2, 'Asistir a la reunión de planificación del proyecto', 'Participar en la reunión de planificación para definir los objetivos y asignar tareas.', '2023-11-05', 'pendiente');

insert into users (username, contrasena) values
("jose", "pass");

insert into lists (listname, user_id) values ("default",1);

INSERT INTO tasks (list_id, title, description, due_date, status) 
VALUES 
	 (1, 'Preparar presentación para reunión de equipo', 'Revisar los datos y preparar diapositivas para la presentación del proyecto.', '2023-09-20', 'pendiente'),
  (1, 'Enviar informe mensual a clientes', 'Generar y enviar el informe de progreso mensual a los clientes principales.', '2023-09-25', 'pendiente'),
  (1, 'Investigar nuevas tendencias de mercado', 'Realizar una investigación sobre las tendencias actuales del mercado y presentar hallazgos.', '2023-09-30', 'en progreso'),
  (1, 'Reunión con el departamento de ventas', 'Coordinar una reunión con el equipo de ventas para discutir estrategias y objetivos.', '2023-10-05', 'pendiente'),
  (1, 'Actualizar la documentación del proyecto', 'Revisar y actualizar la documentación del proyecto con los últimos cambios y mejoras.', '2023-10-10', 'pendiente'),
  (1, 'Entrenamiento de nuevos empleados', 'Conducir sesiones de entrenamiento para los nuevos miembros del equipo.', '2023-10-15', 'pendiente'),
  (1, 'Revisar propuestas de proveedores', 'Analizar y evaluar las propuestas de los proveedores para la próxima adquisición de equipos.', '2023-10-20', 'pendiente'),
  (1, 'Realizar pruebas de rendimiento del software', 'Conducir pruebas de rendimiento en el nuevo software antes del lanzamiento.', '2023-10-25', 'pendiente'),
  (1, 'Preparar presentación para conferencia', 'Preparar la presentación y los materiales necesarios para la próxima conferencia de la industria.', '2023-10-30', 'pendiente'),
  (1, 'Asistir a la reunión de planificación del proyecto', 'Participar en la reunión de planificación para definir los objetivos y asignar tareas.', '2023-11-05', 'pendiente');
select * from tasks;
select * from lists;
select * from users;

-- ------------------------
drop procedure if exists GetTasksByUserIdAndListName;

DELIMITER //

CREATE PROCEDURE GetTasksByUserIdAndListName(
    IN p_user_id INT,
    IN p_listname VARCHAR(50)
)
BEGIN
    SELECT tasks.id, tasks.title, tasks.description, tasks.due_date, tasks.status, tasks.created_at
    FROM tasks INNER JOIN lists ON lists.id = tasks.list_id WHERE lists.listname = p_listname AND  lists.user_id = p_user_id
    ORDER BY tasks.created_at DESC;
END //

DELIMITER ;

CALL GetTasksByUserIdAndListName(1, "default");

-- -----------------------------------------
 
 -- ---------------------------------
DELIMITER //

CREATE PROCEDURE InsertUser(
    IN p_username VARCHAR(50),
    IN p_contrasena VARCHAR(100)
)
BEGIN
    INSERT INTO users (username, contrasena)
    VALUES (p_username, p_contrasena);
END//

DELIMITER ;

-- --------------------------------------------------------
drop procedure if exists GetListNamesByUserId;


DELIMITER //

CREATE PROCEDURE GetListNamesByUserId(
    IN p_user_id INT
)
BEGIN
    SELECT lists.listname, lists.id
    FROM lists
    WHERE user_id = p_user_id;
END //

DELIMITER ;

CALL GetListNamesByUserId(1);

-- ----------------------------------------------

DELIMITER //

CREATE PROCEDURE InsertList(
    IN p_listname VARCHAR(50),
    IN p_user_id INT
)
BEGIN
    INSERT INTO lists (listname, user_id)
    VALUES (p_listname, p_user_id);
END//

DELIMITER ;



-- ------------------------------
drop procedure if exists InsertTaskIntoList;
DELIMITER //

CREATE PROCEDURE InsertTaskIntoList(
    IN p_user_id INT,
    IN plist_name VARCHAR(50),
    IN ptask_title VARCHAR(255),
    IN ptask_description TEXT,
    IN ptask_due_date DATE,
    IN ptask_status ENUM('pendiente', 'en progreso', 'completada')
)
BEGIN
    DECLARE list_id INT;
    
    -- Obtener el ID de la lista
    SELECT lists.id INTO list_id
    FROM lists
    WHERE user_id = p_user_id AND listname = plist_name;
    
    -- Insertar la tarea en la lista
   
	INSERT INTO tasks (list_id, title, description, due_date, status) 
    VALUES (list_id, ptask_title, ptask_description, ptask_due_date, ptask_status);
    

END //

DELIMITER ;


CALL InsertTaskIntoList(1,'default', 'sdada', '', '2023-09-17', 'pendiente');
-- --------------------------------------------------------

-- ------------------------------------------------------
DELIMITER //

CREATE PROCEDURE DeleteTask(IN task_id INT)
BEGIN
    DELETE FROM tasks WHERE id = task_id;
END//

DELIMITER ;

-- ------------------------------------------------------

drop procedure if exists UpdateTaskStatus;
DELIMITER //

CREATE PROCEDURE UpdateTaskStatus(
    IN p_task_id INT,
    IN p_new_status ENUM('pendiente', 'en progreso', 'completada')
)
BEGIN
    UPDATE tasks
    SET status = p_new_status
    WHERE id = p_task_id;
END //

DELIMITER ;

call UpdateTaskStatus(1, "completada");

-- ------------------------------------------------------
drop procedure if exists GetTasksByUserIdListNameAndDate;
DELIMITER //

CREATE PROCEDURE GetTasksByUserIdListNameAndDate(
    IN p_user_id INT,
    IN p_listname VARCHAR(50),
    IN p_due_date DATE
)
BEGIN
    SELECT t.*
    FROM tasks t
    inner JOIN lists l ON t.list_id = l.id
    WHERE l.user_id = p_user_id AND l.listname = p_listname AND t.due_date = p_due_date;
END //

DELIMITER ;

CALL GetTasksByUserIdListNameAndDate(2, 'default', '2023-09-20');

-- ------------------------------------------------------

select * from tasks inner join lists on lists.id = tasks.list_id where users.id = 1 and tasks.title = "";
