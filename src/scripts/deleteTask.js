export const deleteTask = async (task) => {
  const deled = await window.electronAPI.deleteTask(task.dataset.id)
  if (deled) {
    task.remove()
  }
}
