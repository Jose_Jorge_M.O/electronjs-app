import { getTasksList } from './todoList.js'

export const completeTask = async (taskID) => {
  const data = {
    taskId: taskID,
    status: 'completada'
  }
  await window.electronAPI.updateTaskStatus(data)
  const list = document.getElementById('taskList')
  list.remove()
  const listTask = document.createElement('ul')
  const addButton = document.querySelector('[data-list="listTaskSection"]')
  listTask.id = 'taskList'
  getTasksList(listTask)
  addButton.insertBefore(listTask, addButton.firstChild)
}
