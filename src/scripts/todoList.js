import { taskCard } from '../templates/taskCard.js'

const taskListElementHtml = document.getElementById('taskList')

document.addEventListener('DOMContentLoaded', async () => {
  await getTasksList(taskListElementHtml)
})

export const getTasksList = async (taskListElementHtml) => {
  const tasks = await window.electronAPI.getTasks()
  tasks.forEach((task) => {
    const taskElement = taskCard(
      task.title,
      task.description,
      task.due_date,
      task.status,
      task.id
    )

    taskListElementHtml.appendChild(taskElement)
  })
}
