/* eslint-disable no-undef */
import { modalElement } from '../templates/modal.js'

const element = document.getElementById('login_form')

element.addEventListener('submit', async (event) => {
  event.preventDefault()
  const elements = Object.fromEntries(new window.FormData(event.target))

  const userCredentials = {
    username: elements.username,
    password: elements.password
  }

  try {
    await window.electronAPI.login(userCredentials)
    removeModalElement()
    element.appendChild(modalElement('Login success', false))
    const modal = new bootstrap.Modal(document.getElementById('modal'))
    modal.show()
    setTimeout(() => {
      window.location.href = '../html/todoList.html'
    }, 500)
  } catch (error) {
    removeModalElement()
    element.appendChild(modalElement(error.message.split(':')[2], true))
    const modal = new bootstrap.Modal(document.getElementById('modal'))
    modal.show()
  }
})

const removeModalElement = () => {
  const modalElementHtml = document.getElementById('modal')

  if (modalElementHtml) {
    modalElementHtml.remove()
  }
}
