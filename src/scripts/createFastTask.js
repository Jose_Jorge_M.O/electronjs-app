/* eslint-disable no-undef */
import { modalElement } from '../templates/modal.js'
import { taskCard } from '../templates/taskCard.js'

const formFastTask = document.getElementById('fastTasks')

const getDateTask = () => {
  const currentDate = new Date() // Cambiado de FormData a Date
  const year = currentDate.getFullYear()
  const month = currentDate.getMonth() + 1 // Los meses empiezan en 0, por lo que se agrega 1
  const day = currentDate.getDate()

  const formattedDate = `${year}-${month < 10 ? '0' : ''}${month}-${
    day < 10 ? '0' : ''
  }${day}`
  return formattedDate
}
formFastTask.appendChild(modalElement("title can't not be empty", true))
formFastTask.addEventListener('submit', async (event) => {
  event.preventDefault()
  const formData = new FormData(event.target)
  const { task } = Object.fromEntries(formData)
  const dateTask = getDateTask()
  if (task === '') {
    const modal = new bootstrap.Modal(document.getElementById('modal'))
    modal.show()
    return null
  }

  const dataTask = {
    list: 'default',
    title: task,
    description: ' ',
    date: dateTask,
    status: 'pendiente'
  }

  taskCard()

  await window.electronAPI.createTask(dataTask)
  const listTask = document.getElementById('taskList')
  const tasks = await window.electronAPI.getTasks()
  const taskInfo = tasks[0]
  const taskElement = taskCard(
    taskInfo.title,
    taskInfo.description,
    taskInfo.due_date,
    taskInfo.status,
    taskInfo.id
  )
  listTask.insertBefore(taskElement, listTask.firstChild)
})
