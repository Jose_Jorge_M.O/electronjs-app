const nextButton = document.getElementById('nextButton')
const prevButton = document.getElementById('prevButton')

prevButton.disabled = true

nextButton.addEventListener('click', (event) => {
  const slideActive = document.querySelector('.active')
  const slide = slideActive.dataset.slide
  if (slide === '1') {
    event.target.disabled = true
    prevButton.disabled = false
  }
})

prevButton.addEventListener('click', (event) => {
  const slideActive = document.querySelector('.active')
  const slide = slideActive.dataset.slide
  if (slide === '2') {
    event.target.disabled = true
    nextButton.disabled = false
  }
})
