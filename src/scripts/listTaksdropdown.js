import { listElementHtml } from '../templates/listsElement.js'
const btnheader = document.getElementById('btnheader')
const dropdownMenu = document.getElementById('dropdown-menu-lists')

let listsGlobal = null

btnheader.addEventListener('click', async () => {
  const lists = await window.electronAPI.getListsTasksUser()
  if (JSON.stringify(lists) !== JSON.stringify(listsGlobal)) {
    lists[0].forEach((list) => {
      const newNode = listElementHtml(list.listname, list.id)
      dropdownMenu.insertBefore(newNode, dropdownMenu.lastElementChild)
    })
    listsGlobal = lists
  }
})
