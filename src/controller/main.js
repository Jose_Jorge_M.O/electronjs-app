const { app, BrowserWindow, ipcMain } = require('electron')
const { TaskManager } = require('../database/TasksManager.js')
const { UserManager } = require('../database/UserManager.js')
const path = require('path')
require('electron-reload')(__dirname)

const taskManager = new TaskManager()
const userManager = new UserManager(null)

const createWindow = (urlFile) => {
  const window = new BrowserWindow({
    width: 800,
    height: 600,
    // ?here i can communicate node js with the frontend
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })
  window.loadFile(urlFile)
  return window
}

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
})

app.whenReady().then(() => {
  const mainWindow = createWindow('src/views/html/index.html')

  const logoutUser = async (event) => {
    userManager.setUserID(null)
    mainWindow.loadFile('src/views/html/index.html')
  }

  ipcMain.handle('login', async (event, credentials) => {
    userManager.checkUserCredentials(credentials)
  })
  ipcMain.on('setUserID', async (event, id) => {
    userManager.setUserID(id)
  })
  ipcMain.handle('getTasks', async (event) => {
    return await taskManager.getTasksList(userManager.getUSerID())
  })
  ipcMain.on('logout', logoutUser)
  ipcMain.handle('createTask', async (event, dataTask) => {
    console.log('sdas')
    dataTask.userID = userManager.getUSerID()
    console.log(dataTask.userID)
    return await taskManager.createTask(dataTask)
  })
  ipcMain.handle('deleteTask', async (event, dataTask) => {
    return await taskManager.deleteTask(dataTask)
  })

  ipcMain.handle('updateTask', async (event, dataTask) => {
    return taskManager.updateTaskStatus(dataTask.taskId, dataTask.status)
  })

  ipcMain.handle('getListsTasksUser', async (event) => {
    const results = await userManager.getUserLists(userManager.getUSerID())
    return results
  })

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})
