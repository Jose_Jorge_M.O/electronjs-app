const { contextBridge, ipcRenderer } = require('electron')

contextBridge.exposeInMainWorld('electronAPI', {
  login: async (credentials) => await ipcRenderer.invoke('login', credentials),
  setUserID: async (id) => await ipcRenderer.send('setUserID', id),
  logout: async () => await ipcRenderer.send('logout'),
  getTasks: async () => await ipcRenderer.invoke('getTasks'),
  createTask: async (dataTask) =>
    await ipcRenderer.invoke('createTask', dataTask),
  deleteTask: async (dataTask) =>
    await ipcRenderer.invoke('deleteTask', dataTask),
  updateTaskStatus: async (dataTask) =>
    await ipcRenderer.invoke('updateTask', dataTask),
  getListsTasksUser: async () => await ipcRenderer.invoke('getListsTasksUser')
})
