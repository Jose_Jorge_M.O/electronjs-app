const formateDate = (DateString) => {
  const date = new Date(DateString)

  const options = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  }

  const formattedDate = date.toLocaleDateString('es-CO', options)

  return formattedDate
}

const reFormatDate = (dateString) => {
  const dateParts = dateString.split(' ')

  const day = dateParts[1]
  const monthText = dateParts[3]
  const monthNumber =
    new Date(Date.parse(`${monthText} 1, 2023`)).getMonth() + 1
  const year = dateParts[5]

  return `${year}-${monthNumber}-${day}`
}

module.exports = { formateDate, reFormatDate }
