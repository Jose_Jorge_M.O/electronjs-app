const { connect } = require('./databaseConnection.js')

class UserManager {
  constructor(userID) {
    this.init()
    this.userID = userID
  }

  async init() {
    this.connection = await connect()
  }

  async getUserLists(userID) {
    return await this.connection.query('CALL GetListNamesByUserId(?)', [userID])
  }

  async getUsers() {
    return await this.connection.query('SELECT * FROM users')
  }

  async checkUserCredentials(credentials) {
    const result = await this.connection.query(
      'SELECT * FROM users WHERE username= ? AND contrasena= ?',
      [credentials.username, credentials.password]
    )
    if (result.length > 0) {
      this.userID = result[0].id
      return result
    } else {
      throw new Error('Usuario o contraseña incorrectos')
    }
  }

  setUserID(id) {
    this.userID = id
  }

  getUSerID() {
    return this.userID
  }
}

module.exports = { UserManager }
