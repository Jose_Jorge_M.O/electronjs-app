const { connect } = require('./databaseConnection.js')
const { formateDate } = require('../utilities/formatDate.js')

class TaskManager {
  constructor() {
    this.init()
  }

  async init() {
    this.connection = await connect()
  }

  async getTasksList(id) {
    const connection = this.connection
    let todoList = await connection.query(
      'CALL GetTasksByUserIdAndListName(' + id + ', "default")'
    )
    todoList = todoList[0].map((todo) => {
      todo.due_date = formateDate(todo.due_date)
      return todo
    })
    return todoList
  }

  async createTask({ userID, list, title, description, date, status }) {
    console.log('sdfdasfsdf')
    const connection = await this.connection
    try {
      await connection.query('CALL InsertTaskIntoList(?, ?, ?, ?, ?, ?)', [
        userID,
        list,
        title,
        description,
        date,
        status
      ])

      return true
    } catch (Error) {
      console.log('erorr')
      throw Error
    }
  }

  async deleteTask(taskId) {
    const connection = await this.connection
    try {
      await connection.query(`CALL DeleteTask(${taskId})`)
      return true
    } catch (err) {
      throw new Error(err)
    }
  }

  async updateTaskStatus(taskId, status) {
    const connection = await this.connection
    try {
      await connection.query('CALL UpdateTaskStatus(?, ?)', [taskId, status])
      return true
    } catch (err) {
      throw new Error(err)
    }
  }
}

module.exports = { TaskManager }
