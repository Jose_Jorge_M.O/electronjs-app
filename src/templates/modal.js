export const modalElement = (text, buttons) => {
  const div = document.createElement('div')
  const modal = `
    <div class="modal" tabindex="-1" id="modal">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-body">
            <p>${text}</p>
          </div>
          <div class="modal-footer">
            <button type="button" class=" display-${buttons} btn btn-secondary" data-bs-dismiss="modal">Close</button>
            
          </div>
        </div>
      </div>
    </div>
  `

  div.innerHTML = modal

  return div.querySelector('.modal')
}
