export const listElementHtml = (text, id) => {
  const li = document.createElement('li')
  const element = `<a class="dropdown-item" data-id="${id}" data-name="${text}">${text}</a>`
  li.innerHTML = element
  return li
}
