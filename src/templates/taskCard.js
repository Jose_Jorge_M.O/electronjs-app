import { completeTask } from '../scripts/completeTask.js'
import { deleteTask } from '../scripts/deleteTask.js'

export const taskCard = (title, taskInfo, dueDate, status, id) => {
  let btnClass
  if (status === 'completada') {
    btnClass = 'btn-success'
  } else {
    btnClass = 'btn-primary'
  }
  const div = document.createElement('div')
  const card = `<div class="card" data-id="${id}">
  <div class="card-body">
    <h5 class="card-title">${title}</h5>
    <h6 class="card-subtitle mb-2 text-body-secondary">${dueDate}</h6>
    <h6 class="card-subtitle mb-2 text-body-secondary">${status}</h6>
    <p class="card-text">
      ${taskInfo}
    </p>
    <button id="completeBtn" class="btn ${btnClass}">Complete</button>
    <button id="editBtn" class="btn btn-secondary">Edit</button>
  </div>
  <div class="deleteDivButton">
    <button type="button" class="btn btn-danger" id="deleteTaskButton">
      <img class="trashIcon" src="../../assets/icons8-basura.svg">
    </button>
  </div>
</div>`
  div.innerHTML = card
  const taskElement = div.querySelector('.card')
  taskElement
    .querySelector('#deleteTaskButton')
    .addEventListener('click', () => {
      deleteTask(taskElement)
    })

  const completeBtn = div.querySelector('#completeBtn')
  completeBtn.addEventListener('click', async () => {
    if (status !== 'completada') {
      await completeTask(id)
    }
  })
  return taskElement
}
